#ifndef __BUT_EVT_H__
#define __BUT_EVT_H__
#include <stdio.h>
#include "tslib.h"


typedef struct but_evt{
	int area[2][2];
	void *(*func)(void *);
	void *(*func_down)(void *);
//加一个void * 类型用来传参
	struct but_evt *next;
}but_evt;

static but_evt *but_evt_head=NULL;

void evt_register(but_evt * but_evt_p);
void *deal_evt(void *arg);
void init_but_evt();

#endif