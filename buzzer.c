#include "buzzer.h"


int  buzz_switch(){
	int fd = open("/dev/pwm", O_RDWR);
	if(fd < 0)perror("open fail"),exit(-1);
	ioctl(fd, B_ON , 200);
	usleep(1000);
	ioctl(fd,B_OFF, 200);
	close(fd);
	return 0;
}