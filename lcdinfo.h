#ifndef LCDINFO_H
#define LCDINFO_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>


typedef struct LcdDev
{
	int fd;  //打开lcd后得到的文件描述符
	unsigned int *p;//保存映射后的用户空间地址
	unsigned int wscreen;
	unsigned int hscreen;
	unsigned int pixel;
}LcdDev;

//初始化lcd
LcdDev *init_lcd(const char *dev);

//释放lcd资源
bool destroy_lcd(LcdDev *lcd);

//在lcd上绘制一个像素点
bool draw_pix(LcdDev *lcd, int x, int y, unsigned int color);

//在lcd上绘制一行像素
bool draw_line_pix(LcdDev *lcd, int y,unsigned int *color, int length);

int display_jpeg(const char *path);

#endif//LCDINFO_H
