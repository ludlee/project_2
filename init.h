#ifndef __INIT_H_
#define __INIT_H_ 


#include <stdio.h>
#include "but_evt.h"
#include "pthread_pool.h"
#include "lcdinfo.h"
#include "pic.h"
#include "search.h"
#include "buzzer.h"

static D_list * List_Head=NULL;
static D_list * play_tmp=NULL;
static int play_flag=0;
static char play_name_buff[(num+10)];
static char Head_data[num]="0";
int  init_main();
void __key_register();

void *__key_sound(void * arg);
void *__key_rewind(void * arg);
void *__key_play_pause(void * arg);

void *__key_forward(void * arg);

void *__key_stop(void * arg);
void * __key_sound_down(void * arg);
void* __key_rewind_down(void * arg);
void *__key_play_pause_down(void * arg);
void* __key_forward_down(void * arg);
void* __key_stop_down(void * arg);



#endif