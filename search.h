#ifndef __SERCH_PIC__
#define __SERCH_PIC__


#include <stdio.h>
#include <stdbool.h>
#include "klist.h"
#include <sys/mman.h>
#include "jpeglib.h"
#include <setjmp.h>
#include "lcdinfo.h"
#include "Dlink.h"
#include <sys/types.h>
#include <dirent.h>
#include "pic.h"


void search_file(char dirpath[num], const char *type, D_list *List_Head);

#endif