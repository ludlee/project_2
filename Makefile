CC=arm-linux-gcc
TARGET=main

SRCS=$(wildcard *.c) #main.c src.c file.c

OBJS=$(patsubst %.c,%.o,$(SRCS)) #main.o src.o file.o

CONFIG=-I./libjpeg/ -L./libjpeg/ -ljpeg -L/home/ysy/lib/ -lts -lpthread

$(TARGET):$(OBJS)
	$(CC) -o $@  $^  $(CONFIG)
%.o:%.c
	$(CC) -c $< -o $@ $(CONFIG)


clean:
	rm $(TARGET) $(OBJS)
