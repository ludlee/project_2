#include "but_evt.h"


void init_but_evt(){

	but_evt_head=(but_evt *)malloc(sizeof(but_evt));
	but_evt_head->next=but_evt_head;

}

void evt_register(but_evt * but_evt_p){
	but_evt_p->next=but_evt_head->next;
	but_evt_head->next=but_evt_p;
	printf("-----------register\n");	
}


void *deal_evt(void *arg){
	struct tsdev *ts = ts_open("/dev/event0", 0);
	int ret = ts_config(ts);  //如果ret == -1配置失败，0表示成功
	struct ts_sample smp;
	while(1){
		but_evt *p_tmp=but_evt_head->next;
		ret = ts_read(ts, &smp, 1);
		while(p_tmp!=but_evt_head){
			//检测触摸位置是否在注册的按键位置中
			if ((smp.x>=p_tmp->area[0][0])&&(smp.x<=p_tmp->area[1][0])){
				if ((smp.y>=p_tmp->area[0][1])&&(smp.y<=p_tmp->area[1][1])){
					//插入到线程池中太慢了
					// insert_pthread_task(p_tmp->func_down);
					// pthread_pool_doit();
					p_tmp->func_down(NULL);
					usleep(100000);
	//----------------------------------------------------------
					break;
				}
			}			
			p_tmp=p_tmp->next;

		}


		//按键防抖
		while(smp.pressure!=0){

			ts_read(ts, &smp, 1);

		}
		while(p_tmp!=but_evt_head){
			//检测触摸位置是否在注册的按键位置中
			printf("coord:%d,%d\n",smp.x ,smp.y);
			if ((smp.x>=p_tmp->area[0][0])&&(smp.x<=p_tmp->area[1][0])){
				if ((smp.y>=p_tmp->area[0][1])&&(smp.y<=p_tmp->area[1][1])){
//---------------------------------------------------------
					//2.插入任务，参数为函数的指针
					insert_pthread_task(p_tmp->func);
					//p_tmp->func(arg);
					//3.执行调度Do it
					pthread_pool_doit();
//----------------------------------------------------------
					break;
				}
			}			
			p_tmp=p_tmp->next;
		}
	}
	ts_close(ts);	
}
