#include "Dlink.h"



//栈

Stack *create_stack(char data[num]){

	
	return create_kernel_link(data);

}

int pop(Stack * top,char data[num]){
	if (top==NULL){
		perror("Stack top is NULL");
		return -1;
	}
	// if (&(top->list)->next)
	if(((&(top->list))->next)==&(top->list)){
		perror("stack is empty");
		return -1;
	}
	Stack * stack_tmp=container_of((&(top->list))->next, Stack, list);
	strcpy(data,stack_tmp->data);
	// strcpy(data,container_of((&(top->list))->next, Stack, list)->data);
	list_del((&(top->list))->next);
	free(stack_tmp);
	return 0;
}

bool push(Stack *top, char data[num]){
	Stack *stack_tmp=create_stack(data);
	list_add(&stack_tmp->list, &top->list);
	return true;

}

//内核链表

D_list *create_kernel_link(char data[num]){

	D_list *Plist=(D_list *)malloc(sizeof(D_list));
	INIT_LIST_HEAD(&(Plist->list));
	strcpy(Plist->data,data);
	return Plist;

}


bool insert_into_kernel_link(D_list *head,char data[num]){
	D_list * Plist_tmp= create_kernel_link(data);
	list_add(&Plist_tmp->list, &head->list);
	return true;
}


bool show_kernel_link(D_list *head){
	// struct list_head *p = (&(std->list))->next;
	D_list * tmp= container_of((&(head->list))->next, D_list, list);  
	list_for_each_entry(tmp, &head->list, list)
	{
		printf("%s\n", tmp->data);
	}
	return true;
}
