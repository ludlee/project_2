#ifndef __BUZZER_H__
#define __BUZZER_H__ 

#include <stdio.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define B_ON 1
#define B_OFF 0


int  buzz_switch();

#endif