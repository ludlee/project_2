#ifndef __list_head__
#define __list_head__ 

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include "klist.h"
#include <string.h>
#define num 128

typedef struct D_list
{
	char data[num];
	struct list_head list;	
}D_list;

#define Stack D_list

//栈
Stack *create_stack(char data[num]);

int pop(Stack * top,char data[num]);

bool push(Stack *top, char data[num]);



//内核链表
D_list *create_kernel_link(char data[num]);

bool insert_into_kernel_link(D_list *head,char data[num] );

bool show_kernel_link(D_list *head);

// bool delete_kernel_link(char data[num]);




#endif