#include "init.h"

int  init_main(){
	List_Head=create_kernel_link(Head_data);
	printf("searching start!\n");
	search_file("/", ".mp3",List_Head);
	play_tmp= container_of((&(List_Head->list))->next, D_list, list);
	init_pthread_pool();//初始化线程池
	init_but_evt();//初始化按键事件注册
	__key_register();
	printf("init complite\n");
	return 0;
}


void __key_register(){

//按键sound 静音(暂无功能)
	but_evt *but_evt_buf=(but_evt*)malloc(sizeof(but_evt));
	but_evt_buf->area[0][0]=201;
	but_evt_buf->area[0][1]=360;
	but_evt_buf->area[1][0]=267;
	but_evt_buf->area[1][1]=426;
	but_evt_buf->func=&__key_sound;
	but_evt_buf->func_down=&__key_sound_down;	
	evt_register(but_evt_buf);

//按键rewind 上一曲
	but_evt_buf=(but_evt*)malloc(sizeof(but_evt));
	but_evt_buf->area[0][0]=284;
	but_evt_buf->area[0][1]=360;
	but_evt_buf->area[1][0]=350;
	but_evt_buf->area[1][1]=426;
	but_evt_buf->func=&__key_rewind;
	but_evt_buf->func_down=&__key_rewind_down;
	evt_register(but_evt_buf);

//按键play/pause 开启mp3/暂停
	but_evt_buf=(but_evt*)malloc(sizeof(but_evt));
	but_evt_buf->area[0][0]=367;
	but_evt_buf->area[0][1]=360;
	but_evt_buf->area[1][0]=433;
	but_evt_buf->area[1][1]=426;
	but_evt_buf->func=&__key_play_pause;
	but_evt_buf->func_down=&__key_play_pause_down;
	evt_register(but_evt_buf);
//按键forward 下一曲
	but_evt_buf=(but_evt*)malloc(sizeof(but_evt));
	but_evt_buf->area[0][0]=450;
	but_evt_buf->area[0][1]=360;
	but_evt_buf->area[1][0]=516;
	but_evt_buf->area[1][1]=426;
	but_evt_buf->func=&__key_forward;
	but_evt_buf->func_down=&__key_forward_down;
	evt_register(but_evt_buf);
//按键stop 退出
	but_evt_buf=(but_evt*)malloc(sizeof(but_evt));
	but_evt_buf->area[0][0]=500;
	but_evt_buf->area[0][1]=360;
	but_evt_buf->area[1][0]=566;
	but_evt_buf->area[1][1]=426;
	but_evt_buf->func=&__key_stop;
	but_evt_buf->func_down=&__key_stop_down;
	evt_register(but_evt_buf);
	deal_evt(NULL);
}




void * __key_sound(void * arg){

	LcdDev *lcd = init_lcd("/dev/fb0");
	show_jpeg_file(lcd,"./icon/sound.jpg",201,360);//绘制sound
	printf("key_sound pressed down\n");

}
void * __key_sound_down(void * arg){

	LcdDev *lcd = init_lcd("/dev/fb0");
	buzz_switch();
	show_jpeg_file(lcd,"./icon/sound_on.jpg",201,360);//绘制sound

}

void* __key_rewind(void * arg){
	LcdDev *lcd = init_lcd("/dev/fb0");
	show_jpeg_file(lcd,"./icon/rewind.jpg",284,360);
	printf("key_rewind pressed down \n");
		play_tmp= container_of((&(play_tmp->list))->prev, D_list, list);
	if(play_tmp==List_Head){
		play_tmp= container_of((&(play_tmp->list))->prev, D_list, list);
	}
	sprintf(play_name_buff,"madplay %s",play_tmp->data);
	system("killall -9 madplay");
	system(play_name_buff);
	play_flag=1;
}
void* __key_rewind_down(void * arg){
	LcdDev *lcd = init_lcd("/dev/fb0");
	buzz_switch();
	show_jpeg_file(lcd,"./icon/rewind_on.jpg",284,360);
}

void *__key_play_pause(void * arg){
	// LcdDev *lcd = init_lcd("/dev/fb0");
	if (play_flag==0)	{
		sprintf(play_name_buff,"madplay %s",play_tmp->data);
		system(play_name_buff);
		play_flag=1;
		// show_jpeg_file(lcd,"./icon/play.jpg",367,360);
	}else if(play_flag==1){
		// show_jpeg_file(lcd,"./icon/pause.jpg",367,360);
		system("killall -19 madplay");
		play_flag=2;
	}else{
		system("killall -18 madplay");
		play_flag=1;
	}
	printf("key_play_pause pressed down \n");
	//更换图片
}
void *__key_play_pause_down(void * arg){
	LcdDev *lcd = init_lcd("/dev/fb0");
	buzz_switch();
	if (play_flag==0/*未播放*/)	{
		show_jpeg_file(lcd,"./icon/play_on.jpg",367,360);
		
	}else if(play_flag==1/*正在播放音乐*/){
		show_jpeg_file(lcd,"./icon/pause_on.jpg",367,360);
		
	}else{/*音乐暂停*/
		show_jpeg_file(lcd,"./icon/play_on.jpg",367,360);
		
	}
	
}


void* __key_forward(void * arg){
	LcdDev *lcd = init_lcd("/dev/fb0");
	show_jpeg_file(lcd,"./icon/forward.jpg",450,360);
	printf("key_forward pressed down \n");
	play_tmp= container_of((&(play_tmp->list))->next, D_list, list);
	if(play_tmp==List_Head){
		play_tmp= container_of((&(play_tmp->list))->next, D_list, list);
	}
	sprintf(play_name_buff,"madplay %s",play_tmp->data);
	system("killall -9 madplay");
	system(play_name_buff);
	play_flag=1;
}
void* __key_forward_down(void * arg){
	
	LcdDev *lcd = init_lcd("/dev/fb0");
	buzz_switch();
	show_jpeg_file(lcd,"./icon/forward_on.jpg",450,360);
}


void* __key_stop(void * arg){
	LcdDev *lcd = init_lcd("/dev/fb0");
	show_jpeg_file(lcd,"./icon/stop.jpg",533,360);
	printf("key_stop pressed down \n");
	system("killall -9 madplay");
	exit(1);
}
void* __key_stop_down(void * arg){
	LcdDev *lcd = init_lcd("/dev/fb0");
	buzz_switch();
	show_jpeg_file(lcd,"./icon/stop_on.jpg",533,360);
}

