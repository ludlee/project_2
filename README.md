
# 项目二 MP3播放器


作者:杨胜元 ，[其他阅读地址](https://www.zybuluo.com/rfish/note/160055)

---




##1.功能介绍

播放器基于调用madplay实现音乐播放。

###1.1 功能

|按键|功能|
|:-:|:-:|
|![sound.jpg][1]|无功能|
|![rewind.jpg][2]|上一曲|
|![play.jpg][3]|播放|
|![pause.jpg][4]|暂停|
|![forward.jpg][5]|下一曲|
|![stop.jpg][6]|退出|

###1.2 程序流程


```flow
st=>start: 开机
op=>operation: 绘制界面
op2=>operation: 搜索图片
op3=>operation: 初始化线程池
op4=>operation: 初始化按键时间注册
op5=>operation: 注册按键事件
end=>end: 主函数死循环不退出

st->op->op3->op4->op5->end
```

##2.重要数据结构
###**2.1 but_evt.c**

```c
typedef struct but_evt{
    int area[2][2];
    void *(*func)(void *);
    void *(*func_down)(void *);
//可加一个void * 类型用来传参
    struct but_evt *next;
}but_evt;

```

|名称|参数|值|说明|
|:-:|:-:|:-:|:-:|
|`area[0][0]`|无|int|矩形按键区域左上角x轴坐标|
|`area[0][1]`|无|int|矩形按键区域左上角y轴坐标|
|`area[1][0]`|无|int|矩形按键区域右下角x轴坐标|
|`area[1][1]`|无|int|矩形按键区域右下角Y轴坐标|
|`void (*func)(void*)`|void \*|void \*|按键事件函数指针|
|`void *(*func_down)(void *)`|void \*|void \*|按键按下事件函数指针|


###**2.2 pthread_pool**

```c
typedef struct struct_pthread_pool{
    pthread_t pthread_id;
    struct struct_pthread_pool *next;
}struct_pthread_pool;
```
|名称|参数|值|说明|
|:-:|:-:|:-:|:-:|
|pthread\_t pthread\_id;|无|pthread\_t|线程池空闲线程id，方便监控的守护进程杀死过多的进程|



##3. 文件说明


|文件名|存储内容|说明|
|:-:|:-:|:-:|
|~~.but_evt~~|~~按键事件注册链表~~|暂未添加|
|.path|mp3搜索出来的路径||


注意：icon文件必须放在程序同目录下

```
./
├── icon
│   ├── background.jpg
│   ├── forward.jpg
│   ├── forward_on.jpg
│   ├── pause.jpg
│   ├── pause_on.jpg
│   ├── play.jpg
│   ├── play_on.jpg
│   ├── rewind.jpg
│   ├── rewind_on.jpg
│   ├── sound.jpg
│   ├── sound_on.jpg
│   ├── stop.jpg
│   └── stop_on.jpg
└── main


```


##4. madplay

>应用中使用system调用madplay播放音频后，无法直接控制暂停/停止等操作，直接对madplay进程进行控制

>- `"killall -19 madplay"`使进程挂起以暂停
>- `"killall -18 madplay"`使进程恢复运行
>- `"killall -9 madplay"`终止进程以停止。 


  [1]: ./icon/sound.jpg
  [2]: ./icon/rewind.jpg
  [3]: ./icon/play.jpg
  [4]: ./icon/pause.jpg
  [5]: ./icon/forward.jpg
  [6]: ./icon/stop.jpg
  [7]: ./icon/play.jpg
  [8]: ./icon/rewind.jpg
  [9]: ./icon/play.jpg