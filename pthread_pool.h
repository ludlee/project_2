
#ifndef __PTHREAD_POOL_H__
#define __PTHREAD_POOL_H__ 

#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdlib.h>


#define MAX_PTHREAD 10
#define MIN_PTHREAD 2
#define THRESHOLD 8 //应该大于2

static int pthread_all_num=0;
/**
 * 线程任务列表
 */
typedef struct struct_pthread_task{
	void* (*func)(void *);
	//加一个void * 类型用来传参
	struct struct_pthread_task * next;
}struct_pthread_task;

/**
 * 保存线程池中空闲线程的id
 * 方便后面杀死线程
 */
typedef struct struct_pthread_pool{
	pthread_t pthread_id;
	struct struct_pthread_pool *next;
}struct_pthread_pool;

static struct_pthread_task * struct_pthread_task_head=NULL;
static struct_pthread_pool* struct_pthread_pool_head=NULL;

void *pthread_task(void *arg);
/**
 * 生成一个链表节点
 */
struct_pthread_task * creat_pthread_task (void* (*function)(void *));
struct_pthread_pool * creat_pthread_pool (pthread_t pthread_id);

/**
 * 链表头节点。
 */
pthread_cond_t cont ;
/**
 * [初始化线程池]
 * @return [0成功，-1失败]
 */
int init_pthread_pool();
/**
 * [线程监控，处理线程数过多问题]
 * @param  arg [传入NULL]
 * @return     [NULL]
 */
void * pthread_monitor(void * arg);

int pthread_pool_doit();

void insert_pthread_task(void* (*function)(void *));
void insert_pthread_pool(pthread_t pthread_id);

#endif